# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 16:13:52 2016

@author: David Evans
"""

import numpy as np

def bellmanTmap(beta,w,pi,b,alpha,chi,Vcont,Qcont):
    '''
    Iterates of Bellman Equation in Problem 1
    See Homework 3.pdf for Documentation
    '''
    S = len(w)
    V = np.zeros(S)
    
    Q_nosearch = b+beta*(0.3*pi.dot(Vcont)+0.7*Qcont)
    Q_search = b-chi+beta*(0.5*pi.dot(Vcont)+0.5*Qcont)
    Q = np.maximum(Q_nosearch,Q_search)
    
    V_employed = w+beta*(alpha*Qcont+(1-alpha)*Vcont)
    V_unemployed = Q*np.ones(S)
    V = np.maximum(V_employed, V_unemployed)
    
    C = np.ones(S)
    for s in range(S):
        if V_unemployed[s]==V[s]:
            C[s]=0
    
    E = (Q==Q_search)
    
    return V,Q,C,E
    
    
    
def solveInfiniteHorizonProblem(beta,w,pi,b,alpha,chi,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem in Problem 1
    See Homework 3.pdf for Documentation
    '''
    S=len(w)
    V=np.zeros(S)
    Q=0
    Vnew,Qnew,C,E = bellmanTmap(beta,w,pi,b,alpha,chi,V,Q)
    while np.linalg.norm(V-Vnew)>epsilon:
        V=Vnew
        Q=Qnew
        Vnew,Qnew,C,E = bellmanTmap(beta,w,pi,b,alpha,chi,V,Q)
    return Vnew, Qnew, C, E
    
def HazardRate(pi,C,E):
    '''
    Computes the hazard rate of leaving unemployment in Problem 1
    See Homework 3.pdf for Documentation
    '''
    if E:
        wage_prob = .5
    else:
        wage_prob = .3
    
    Hrate = wage_prob*(pi.dot(C))
    return Hrate
    

        
        
def bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,Vcont,Qcont):
    '''
    Iterates of Bellman Equation of Problem 2
    See Homework 3.pdf for Documentation
    '''
    
    J = len(Qcont)
    S = len(Vcont[1,:])
    
    Q = np.zeros(J)
    V = np.zeros((J,S))
    Vemp = np.zeros((J,S))
    C = np.zeros((J,S))
        
    
    Q[0] = b + beta*(pi.dot(Vcont[0,:]))
    for j in range(1,J):
        Q[j] = b + beta*(p_h*(pi.dot(Vcont[j-1,:]))+(1-p_h)*(pi.dot(Vcont[j,:])))
        
    for j in range(J-1):
        V_if_fired = p_h*Qcont[j+1] + (1-p_h)*Qcont[j]
        V_if_not   = p_h*Vcont[j+1,:] + (1-p_h)*Vcont[j,:]
        Vemp[j,:] = w*h[j] + beta*(alpha*V_if_fired + (1-alpha)*V_if_not)
        V[j,:] = np.maximum(Vemp[j,:],np.ones(S)*Q[j])
        
    V_if_fired = p_h*Qcont[J-1] + (1-p_h)*Qcont[J-1]
    V_if_not   = p_h*Vcont[J-1,:] + (1-p_h)*Vcont[J-1,:]
    Vemp[J-1,:] = w*h[J-1] + beta*(alpha*V_if_fired + (1-alpha)*V_if_not)
    V[J-1,:] = np.maximum(Vemp[J-1,:],np.ones(S)*Q[J-1])
        
    for j in range(J):
        for s in range(S):
            if V[j,s] == Vemp[j,s]:
                C[j,s] = 1
                
    return V, Q, C
    

def solveInfiniteHorizonProblem_HC(beta,w,pi,b,alpha,p_h,h,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem of Problem 2
    See Homework 3.pdf for Documentation
    '''
    S = len(w)
    J = len(h)
    
    V = np.zeros((J,S))
    Q = np.zeros(J)
    C = np.zeros((J,S))
    
    Vnew, Qnew, Cnew = bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,V,Q)
    
    while (np.linalg.norm(V-Vnew)>epsilon):
        V,Q,C = Vnew,Qnew,Cnew
        Vnew,Qnew,Cnew = bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,V,Q)
    return Vnew,Qnew,Cnew

    
    
    
    
    
    
    
    